import './style.css';
import { receiveHover, cleanHover } from 'modules/hover';
import { requestAnimationFrame } from 'modules/raf';

let navigationMethod;

export class NavigationDropdown {
  static setRouteMethod(method) {
    navigationMethod = method;
  }

  constructor({
    items, popupType, attachTo, position,
    onChange, onOpen, onClose, className
  }) {
    this.items = items.map(({ url, text, selected, data }, index) => {
      if (selected) {
        this.selectedIndex = index;
      }

      return { url, text, data };
    });

    this.closed = true;

    this.selectedItem = null;
    this.popupType = popupType || 'absolute';
    this.position = position || 'top left';
    this.attachTarget = attachTo;
    this.className = className;

    this.triggers = [];

    this.onChange = onChange;
    this.onOpen = onOpen;
    this.onClose = onClose;
  }

  open(element = this.attachTarget, position = this.position) {
    if (!element) {
      throw new Error('Attach target is required');
    }

    if (!this.closed) {
      return;
    }

    this.closed = false;
    this.animating = true;

    if (this._closingTimer) {
      clearTimeout(this._closingTimer);
      this._closingTimer = null;
    }

    if (!this.node) {
      this._init();
    }

    const rect = element.getBoundingClientRect();
    let left;
    let top;

    switch (this.popupType) {
      case 'absolute': {
        left = rect.left + pageXOffset;
        top = rect.top + pageYOffset;
      } break;
      case 'fixed': {
        left = rect.left;
        top = rect.top;
      } break;
      case 'inline': {
        left = 0;
        top = 0;
      } break;
    }

    switch (position) {
      case 'top left': {
        this.node.style.left = left + 'px';
        this.node.style.top = top + 'px';
        this.node.setAttribute('data-translate', '');
      } break;
      case 'top right': {
        this.node.style.left = left + rect.width + 'px';
        this.node.style.top = top + 'px';
        this.node.setAttribute('data-translate', '-1 0');
      } break;
      case 'bottom left': {
        this.node.style.left = left + 'px';
        this.node.style.top = top + rect.height + 'px';
        this.node.setAttribute('data-translate', '0 -1');
      } break;
      case 'bottom right': {
        this.node.style.left = left + rect.width + 'px';
        this.node.style.top = top + rect.height + 'px';
        this.node.setAttribute('data-translate', '-1 -1');
      } break;
    }

    this._monitorClosing();

    if (this.popupType === 'inline') {
      element.parentNode.appendChild(this.node);
    } else {
      document.body.appendChild(this.node);
    }

    requestAnimationFrame(() => {
      // This happens right before the painting of
      // the frame with popup inserting

      requestAnimationFrame(() => {
        // This happens right before the next frame painting

        this.node.setAttribute('data-visible', '');
      });
    });
  }

  close({ immediate }) {
    if (this.closed) return;

    if (immediate) {
      this.closed = true;
      this.node.parentNode.removeChild(this.node);
      this._clean();
    } else {
      this._doClose();
    }
  }

  getSelected() {
    return this.items[this.selectedIndex];
  }

  removeItem(index) {
    if (index < 0 || index >= this.items.length) {
      return;
    }

    this.items.splice(index, 1);

    if (this.node) {
      const child = this.node.children[index];

      cleanHover(child);
      this.node.removeChild(child);
    }

    if (this.selectedIndex === index) {
      this.selectedIndex = void 0;
      this.selectedItem = void 0;
    }
  }

  addItem({ url, text, selected, data }, index) {
    if (!isFinite(index) || index > this.items.length) {
      index = this.items.length
    } else if (index < 0) {
      index = 0;
    }

    if (selected) {
      this.selectedIndex = index;
    } else if (this.selectedIndex >= index) {
      this.selectedIndex += 1;
    }

    const item = { url, text, data };
    const link = this._createLink(item, index);

    if (this.selectedIndex === index) {
      link.setAttribute('data-selected', '');
      this.selectedItem = link;
    }

    this.items.splice(index, 0, item);

    if (this.node) {
      this.node.insertBefore(link, this.node.children[index]);
    }
  }

  selectItem(index, params) {
    if (
      this.selectedIndex === index ||
      index < 0 || index >= this.items.length
    ) {
      return;
    }

    this.selectedIndex = index;

    if (this.selectedItem) {
      this.selectedItem.removeAttribute('data-selected');
    }

    if (this.node) {
      this.selectedItem = this.node.children[index];
      this.selectedItem.setAttribute('data-selected', '');
    }

    if (params && params.event) {
      if (this.onChange) {
        this.onChange(this.items[index], index);
      }
    }
  }

  addTrigger(element, position) {
    const entry = {
      element,
      listener: (e) => {
        e.preventDefault();
        this.open(this.attachTarget, position);
      }
    };

    this.triggers.push(entry);
    element.addEventListener('click', entry.listener);
  }

  setURLs(urls) {
    this.items.some((item, index) => {
      if (index >= urls.length) {
        return true;
      }

      const url = urls[index];

      if (this.node) {
        const link = this.node.children[index];
        link.href = url;
      }

      item.url = url;
    });
  }

  destroy() {
    if (this.node) {
      if (!this.closed) {
        this._clean();
      }

      this.node.removeEventListener('click', this._nodeListener);
      this.node.removeEventListener('touchmove', this._nodeListener);

      [].forEach.call(this.node.children, (child) => {
        cleanHover(child);
      });

      if (this.node.parentNode) {
        this.node.parentNode.removeChild(this.node);
      }

      this.node = null;
    }

    this.triggers.forEach(({ element, listener }) => {
      element.removeEventListener('click', listener);
    });

    this.items = null;
    this.triggers = null;
    this.selectedItem = null;
    this.attachTarget = null;

    this.onChange = null;
    this.onOpen = null;
    this.onClose = null;
  }

  _init() {
    this.node = document.createElement('div');

    this.node.className = `c-nd-box ${this.className || ''}`;
    this.node.setAttribute('data-type', this.popupType);

    this.items.forEach((item, index) => {
      const link = this._createLink(item, index);

      if (this.selectedIndex === index) {
        link.setAttribute('data-selected', '');
        this.selectedItem = link;
      }

      this.node.appendChild(link);
    });

    this.node.addEventListener('click', this._nodeListener);

    if (!window.PointerEvent) {
      this.node.addEventListener('touchmove', this._nodeListener);
    }
  }

  _nodeListener = (e) => {
    if (e.type === 'click') {
      // Ctrl/Cmd key
      if (e.ctrlKey || e.metaKey) {
        return;
      }

      // Not primary button
      if (e.button) {
        return;
      }

      const target = e.target;
      const index = +target.getAttribute('data-index');

      this.close({ immediate: true });
      this.selectItem(index, { event: true });

      if (navigationMethod) {
        e.preventDefault();

        const item = this.items[index];
        navigationMethod(item.url);
      }

      return;
    }

    if (e.type === 'touchmove') {
      e.preventDefault();
    }
  }

  _createLink(item, index) {
    const link = document.createElement('a');
    receiveHover(link);

    link.className = 'c-nd-link';
    link.href = item.url;
    link.textContent = item.text;
    link.setAttribute('data-index', index);

    return link;
  }

  _monitorClosing() {
    document.body.style.touchAction = 'none';
    document.body.style.msTouchAction = 'none';

    // Always prefer Pointer Events
    if (window.PointerEvent) {
      document.addEventListener('pointerdown', this._onClose, true);
    } else {
      document.addEventListener('mousedown', this._onClose, true);
      document.addEventListener('touchstart', this._onClose, true);
    }

    document.addEventListener('keydown', this._onClose, true);
  }

  // Disables any pointer events on the <body> until click happens.
  // This is used instead of just canceling 'click' event to prevent
  // any other intermediate events.
  _monitorClickCancelation() {
    document.body.style.pointerEvents = 'none';

    if (window.PointerEvent) {
      document.addEventListener('pointercancel', this._clickCleaning, true);
      document.addEventListener('pointerdown', this._clickCleaning, true);
    } else {
      document.addEventListener('touchcancel', this._clickCleaning, true);
      document.addEventListener('touchstart', this._clickCleaning, true);
      document.addEventListener('mousedown', this._clickCleaning, true);
    }

    document.addEventListener('contextmenu', this._clickCleaning, true);
    document.addEventListener('click', this._clickCleaning, true);
  }

  _onClose = (e) => {
    if (this.node === e.target || this.node.contains(e.target)) {
      return;
    }

    if (e.type === 'touchstart' && e.touches.length > 1) {
      return;
    }

    // If anything but mouse
    if (
      (e.type === 'pointerdown' && e.pointerType !== 'mouse') ||
      e.type === 'touchstart'
    ) {
      this._monitorClickCancelation();

      e.stopImmediatePropagation();
      e.preventDefault();
    }

    if (e.type === 'keydown' && e.keyCode !== 27) {
      return;
    }

    this._doClose();
  }

  _doClose = () => {
    this.closed = true;
    this._clean();

    // setTimeout is just fine
    this._closingTimer = setTimeout(() => {
      this._closingTimer = null;

      requestAnimationFrame(() => {
        this.node.parentNode.removeChild(this.node);
      });
    }, 1000);
  }

  _clickCleaning = () => {
    document.body.style.pointerEvents = '';

    if (window.PointerEvent) {
      document.removeEventListener('pointercancel', this._clickCleaning, true);
      document.removeEventListener('pointerdown', this._clickCleaning, true);
    } else {
      document.removeEventListener('touchcancel', this._clickCleaning, true);
      document.removeEventListener('touchstart', this._clickCleaning, true);
      document.removeEventListener('mousedown', this._clickCleaning, true);
    }

    document.removeEventListener('contextmenu', this._clickCleaning, true);
    document.removeEventListener('click', this._clickCleaning, true);
  }

  _clean() {
    document.body.style.touchAction = '';
    document.body.style.msTouchAction = '';
    this.node.removeAttribute('data-visible');

    if (window.PointerEvent) {
      document.removeEventListener('pointerdown', this._onClose, true);
    } else {
      document.removeEventListener('mousedown', this._onClose, true);
      document.removeEventListener('touchstart', this._onClose, true);
    }

    document.removeEventListener('keydown', this._onClose, true);
  }
}