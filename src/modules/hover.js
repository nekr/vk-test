export function receiveHover(element) {
  if (window.PointerEvent) {
    element.addEventListener('pointerenter', onEvent);
    element.addEventListener('pointermove', onEvent);
    element.addEventListener('pointerleave', onEvent);
  } else {
    element.addEventListener('mouseenter', onEvent);
    element.addEventListener('mouseleave', onEvent);

    element.addEventListener('touchstart', onEvent);
    element.addEventListener('touchmove', onEvent);
    element.addEventListener('touchend', onEvent);
    element.addEventListener('touchcancel', onEvent);
  }

  element.addEventListener('contextmenu', onEvent);
  element.addEventListener('click', onEvent);
}

export function cleanHover(element) {
  if (window.PointerEvent) {
    element.removeEventListener('pointerenter', onEvent);
    element.removeEventListener('pointermove', onEvent);
    element.removeEventListener('pointerleave', onEvent);
  } else {
    element.removeEventListener('mouseenter', onEvent);
    element.removeEventListener('mouseleave', onEvent);

    element.removeEventListener('touchstart', onEvent);
    element.removeEventListener('touchmove', onEvent);
    element.removeEventListener('touchend', onEvent);
    element.removeEventListener('touchcancel', onEvent);
  }

  element.removeEventListener('contextmenu', onEvent);
  element.removeEventListener('click', onEvent);
}

function onEvent(e) {
  const target = e.currentTarget;

  if (e.type === 'pointerenter' || e.type === 'mouseenter') {
    target.setAttribute('data-hover', '');

    if (e.type === 'pointerenter' && e.pointerType !== 'mouse') {
      target.__hoverStartX__ = e.clientX;
      target.__hoverStartY__ = e.clientY;
    }

    return;
  }

  if (
    e.type === 'pointerleave' || e.type === 'mouseleave' ||
    e.type === 'contextmenu' || e.type === 'click'
  ) {
    target.removeAttribute('data-hover');
    return;
  }

  if (e.type === 'touchstart') {
    target.__hoverStartX__ = e.targetTouches[0].clientX;
    target.__hoverStartY__ = e.targetTouches[0].clientY;

    target.setAttribute('data-hover', '');
    return;
  }

  if (e.type === 'pointermove' || e.type === 'touchmove') {
    const { clientX, clientY } = (e.targetTouches ? e.targetTouches[0] : e);

    if (
      Math.abs(clientX - target.__hoverStartX__) >= 10 ||
      Math.abs(clientY - target.__hoverStartY__) >= 10
    ) {
      target.removeAttribute('data-hover');
    }

    return;
  }

  if (e.type === 'touchend' || e.type === 'touchcancel') {
    if (e.targetTouches.length) {
      return;
    }

    target.removeAttribute('data-hover');
    return;
  }
}