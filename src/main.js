import 'modules/offline';

import assign from 'object-assign';
import URLSearchParams from 'url-search-params';
import { receiveHover, cleanHover } from 'modules/hover';

import { NavigationDropdown } from 'modules/NavigationDropdown';

const loadParams = new URLSearchParams(location.search);

const headerSwitcher = initHeaderSwitcher();
const inlinePopup = initHeaderInlinePopup();
const multiButtons = initContentMultiButtons();
const sortButton = initContentSortButton();

NavigationDropdown.setRouteMethod((url) => {
  history.pushState(null, '', url);
  onURLChange();
});

window.addEventListener('popstate', () => {
  onURLChange();
});

onURLChange();

function initHeaderSwitcher() {
  const headerSwitcher = document.querySelector('#header-switcher');
  const dropdown = new NavigationDropdown({
    items: [
      { text: 'Все сообщения', url: '/',
        selected: !loadParams.get('type') },
      { text: 'Непрочитанные', url: '/',
        selected: loadParams.get('type') === 'unread' },
      { text: 'Важные', url: '/',
        selected: loadParams.get('type') === 'important' },
    ],

    onChange: (item, index) => {
      headerSwitcher.textContent = item.text;
    },

    popupType: 'fixed',
    className: 'app-header__dropdown'
  });

  receiveHover(headerSwitcher);

  headerSwitcher.addEventListener('click', (e) => {
    e.preventDefault();
    dropdown.open(headerSwitcher, 'top left');
  });

  headerSwitcher.textContent = dropdown.getSelected().text;

  return dropdown;
}

function initHeaderInlinePopup() {
  const button = document.querySelector('#inline-popup');
  const dropdown = new NavigationDropdown({
    items: [
      { text: 'This is inline popup', url: '/',
        selected: !loadParams.get('inline') },
      { text: 'Choose something else', url: '/',
        selected: loadParams.get('inline') === '1' },
      { text: 'Not this one', url: '/',
        selected: loadParams.get('inline') === '2' },
    ],

    popupType: 'inline',
    className: 'app-header__dropdown'
  });

  receiveHover(button);

  button.addEventListener('click', (e) => {
    e.preventDefault();
    dropdown.open(button, 'top right');
  });

  return dropdown;
}

function initContentMultiButtons() {
  const target = document.querySelector('#test1-target');
  const dropdown = new NavigationDropdown({
    items: [
      { text: 'Red', url: '/',
        selected: loadParams.get('multi') === 'red', data: 'red' },
      { text: 'Green', url: '/',
        selected: loadParams.get('multi') === 'green', data: 'green' },
      { text: 'Blue', url: '/',
        selected: loadParams.get('multi') === 'blue', data: 'blue' },
    ],

    onChange: (item) => {
      target.style.backgroundColor = item.data;
    },

    popupType: 'absolute',
    attachTo: target
  });

  [].forEach.call(
    document.querySelectorAll('#multi-test [data-trigger]'),
    (elem) => {
      dropdown.addTrigger(elem, elem.getAttribute('data-trigger'));
    }
  );

  const selected = dropdown.getSelected();

  if (selected) {
    target.style.backgroundColor = selected.data;
  }

  return dropdown;
}

function initContentSortButton() {
  const items = [
    { text: 'Popularity', url: '/',
      selected: loadParams.get('sort') === 'popularity' },
    { text: 'Relevance', url: '/',
      selected: loadParams.get('sort') === 'relevance' },
  ];

  const button = document.querySelector('#sort-button');
  const dropdown = new NavigationDropdown({
    items: items.map(item => assign({}, item)),

    onChange: (item, index) => {
      const newItem = assign({}, items[index]);
      newItem.text += ` ${Date.now()}`;
      newItem.selected = true;
      newItem.url = item.url;

      dropdown.addItem(newItem, index);
      dropdown.removeItem(index + 1);
    },

    popupType: 'inline'
  });

  button.addEventListener('click', (e) => {
    e.preventDefault();
    dropdown.open(button, 'bottom left');
  });

  return dropdown;
}

function onURLChange() {
  const params = {};

  new URLSearchParams(location.search).forEach((value, key) => {
    params[key] = value;
  });

  headerSwitcher.setURLs([
    makeURL(params, 'type', null),
    makeURL(params, 'type', 'unread'),
    makeURL(params, 'type', 'important')
  ]);

  inlinePopup.setURLs([
    makeURL(params, 'inline', null),
    makeURL(params, 'inline', '1'),
    makeURL(params, 'inline', '2')
  ]);

  multiButtons.setURLs([
    makeURL(params, 'multi', 'red'),
    makeURL(params, 'multi', 'green'),
    makeURL(params, 'multi', 'blue'),
  ]);

  sortButton.setURLs([
    makeURL(params, 'sort', 'popularity'),
    makeURL(params, 'sort', 'relevance'),
  ]);
}

function makeURL(params, key, value) {
  const newParams = new URLSearchParams(params);

  if (!value) {
    newParams.delete(key);
  } else {
    newParams.set(key, value);
  }

  const query = newParams + '';

  return query ? `/?${query}` : '/';
}