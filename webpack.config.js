const webpack = require('webpack');

const autoprefixer = require('autoprefixer');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OfflinePlugin = require('offline-plugin');

const ProvidePlugin = webpack.ProvidePlugin;
const DefinePlugin = webpack.DefinePlugin;
const UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;

const { resolve, join } = require('path');

const isProduction = process.env.NODE_ENV === 'production';
const APP_DIST_FOLDER = resolve('dist');
const APP_SRC_FOLDER = resolve('src');

const postcssLoader = {
  loader: 'postcss-loader',
  options: {
    plugins: (ctx) => [
      autoprefixer({ browsers: ['>= 1%', 'IE >= 10'] }),
    ]
  }
};

const cssLoader = [
  {
    loader: 'css-loader',
    options: {
      import: false,
      minimize: isProduction
    }
  },
  postcssLoader
];

const extractCSSPlugin = new ExtractTextPlugin({
  filename: '[name].css',
  allChunks: true,
});

module.exports = function(env = {}) {
  const plugins = [];

  if (isProduction) {
    plugins.push(new UglifyJsPlugin({
      compress: {
        warnings: false,
        dead_code: true,
        drop_console: true,
        unused: true
      },

      output: {
        comments: false
      }
    }));
  }

  return {
    entry: {
      main: resolve('src/main.js')
    },
    output: {
      path: resolve(APP_DIST_FOLDER),
      filename: '[name].js',
      chunkFilename: 'chunks/[name].js',
      publicPath: '/'
    },

    module: {
      rules: [
        {
          test: /\.(jpe?g|png|gif|svg|woff2?|otf)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: 'assets/[hash].[ext]'
              }
            }
          ]
        },

        {
          test: /\.(jpe?g|png|gif|svg)$/,
          resourceQuery: /inline/, // foo.svg?inline
          use: 'url-loader'
        },

        {
          use: extractCSSPlugin.extract({
            use: cssLoader,
            fallback: 'style-loader',
          }),
          test: /\.css$/,
          include: [
            resolve('src/modules'),
            resolve('src/styles'),
          ]
        },

        {
          loader: cssLoader,
          test: /\.css$/,
          exclude: [
            resolve('src/modules'),
            resolve('src/styles'),
            resolve('node_modules')
          ]
        },

        {
          test: /\.jsx?$/,
          loader: 'strip-sourcemap-loader'
        },

        {
          loader: 'babel-loader',
          test: /\.jsx?$/,
          exclude: /node_modules/,
          options: {
            cacheDirectory: true,
            presets: [
              ['env', {
                modules: false,
                loose: true,
                targets: {
                  browsers: ['>= 1%', 'IE >= 10']
                }
              }]
            ],
            plugins: [
              'syntax-dynamic-import',
              'transform-class-properties'
            ]
          }
        }
      ]
    },

    plugins: [
      new CleanWebpackPlugin([ APP_DIST_FOLDER ]),
      new HtmlWebpackPlugin({
        template: 'src/index.html',
        filename: 'index.html',
        inject: false,
        minify: isProduction ? {
          removeComments: true,
          removeCommentsFromCDATA: true,
          removeCDATASectionsFromCDATA: true,
          collapseWhitespace: true,
          collapseBooleanAttributes: true,
          minifyJS: {
            compress: {
              warnings: false,
              dead_code: true,
              drop_console: true,
              unused: true
            },

            output: {
              comments: false
            }
          },
          minifyCSS: {}
        } : false
      }),
      extractCSSPlugin,
      new DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
        },
      }),
      new OfflinePlugin({
        ServiceWorker: {
          events: true,
        },
        AppCache: {
          events: true,
          FALLBACK: { '/': '/' }
        }
      })
    ]
    .concat(plugins),

    resolve: {
      modules: [resolve('src'), resolve('node_modules')],
      extensions: ['.js', '.jsx']
    },

    node: {
      console: false,
      process: false,
      global: true,
      buffer: false,
      setImmediate: false
    },
  }
};